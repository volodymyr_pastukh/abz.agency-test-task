import React, { Component } from 'react'
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

import usersAPI from '../../services/usersAPI'
import InputC from './InputC/InputC'
import SelectC from './SelectC/SelectC'
import UploadC from './UploadC/UploadC'
import ButtoC from '../ButtonC/ButtonC'

import CircularProgress from '@material-ui/core/CircularProgress';

import AlertDialog from '../DialogWin/DialogWin'
import './RegisterForm.scss'

export class RegisterForm extends Component {
  constructor(props) {
    super(props) 
    this.state = {
      positions: [{name: 'LoAdInG'}],

      errorHandlerName: false,
      userName: '',
      helperTextName: '⠀',

      errorHandlerEmail: false,
      userEmail: '',
      helperTextEmail: '⠀',

      errorHandlerPhone: false,
      userPhone: '',
      helperTextPhone: '⠀',

      errorHandlerUpload: false,
      userUpload: '',
      helperTextUpload: 'File format jpg  up to 5 MB, the minimum size of 70x70px',
      
      errorHandlerPosition: false,
      userPosition: '',

      buttonActive: false,

      text: 'Upload your photo',

      loaderDisplay: 'none',
      buttonDisplay: 'block',

      open: false
    }

    this.blurHandlerName = this.blurHandlerName.bind(this)
    this.blurHandlerEmail = this.blurHandlerEmail.bind(this)
    this.blurHandlerPhone = this.blurHandlerPhone.bind(this)
    this.submitHandler = this.submitHandler.bind(this)
    this.selectHandler = this.selectHandler.bind(this)
    this.uploadHandle = this.uploadHandle.bind(this)
    this.closeDialog = this.closeDialog.bind(this)
    this.openDialog = this.openDialog.bind(this)
  }

  componentDidMount() {
    usersAPI.getPositions().then((pos) => {
      this.setState({
        positions: pos.data.positions
      })
    })
  }

  blurHandlerName(e) {
    let value = e.target.value.trim()
    if(value === ''){
      
      this.setState({
        userName: '',
        errorHandlerName: true,
        helperTextName: 'Enter your name'
      })
    }else {
      this.setState({
        userName: e.target.value,
        errorHandlerName: false,
        helperTextName: '⠀'
      })
    }
  }

  blurHandlerEmail(e) {
    let value = e.target.value.trim();
    // let re = 0; use for check proj console
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(value === '' || !re.test(String(value).toLowerCase())){
      this.setState({
        userEmail: '',
        errorHandlerEmail: true,
        helperTextEmail: 'Use format example@abz.com'
      })
    }else {
      this.setState({
        userEmail: e.target.value,
        errorHandlerEmail: false,
        helperTextEmail: '⠀'
      }, )
    }
  }

  blurHandlerPhone(e) {
    let value = e.target.value.replace(/\s+/g, '').replace(/[()]/g, '').replace(/[_]/g, '').trim()
    if(value.length < 13){
      this.setState({
        userPhone: '',
        errorHandlerPhone: true,
        helperTextPhone: 'Use format 999 999 9999'
      })
    }else {
      this.setState({
        userPhone: value,
        errorHandlerPhone: false,
        helperTextPhone: '⠀'
      })
    };
  }

  submitHandler() {
      if(this.state.userName === '') {
        this.setState({
          errorHandlerName: true,
          helperTextName: 'Enter name'
        })
      } else {
        if(this.state.userEmail === '') {
          this.setState({
            errorHandlerEmail: true,
            helperTextEmail: 'Use format example@abz.com'
          })
        } else{
          if(this.state.userPhone === '') {
            this.setState({
              errorHandlerPhone: true,
              helperTextPhone: 'Use format 999 999 9999'
            })
          } else {
          if(this.state.userPosition === '') {
            this.setState({
              errorHandlerPosition: true,
              helperTextPosition: 'Select your position'
            })
          } else {
            if(this.state.userUpload === '') {
              this.setState({
                errorHandlerUpload: true,
              })
            }
            else {
              let formData = new FormData();
              formData.append('position_id', 2);
              formData.append('name', this.state.userName);
              formData.append('email', this.state.userEmail);
              formData.append('phone', this.state.userPhone);
              formData.append('photo', this.state.userUpload);

              this.setState({
                loaderDisplay: 'block',
                buttonDisplay: 'none',
              })

              usersAPI.getToken().then((res)=>(
                fetch('https://frontend-test-assignment-api.abz.agency/api/v1/users', {
                  method: 'POST',
                  body: formData,
                  headers: {
                    'Token': res.data.token, // get token with GET api/v1/token method
                  },
                })
                .then(function(response) {
                  
                  return response.json();
                })
                .then(function(data) {
                  if(data.success) {
                  } else {
                  }
                })
                .catch(function(error) {
                }).then(()=>{
                  this.setState({
                    loaderDisplay: 'none',
                    buttonDisplay: 'block',
                  } )
                  this.openDialog()
                })
              ))
            }
          }
        } 
      }
    }
  }

  selectHandler(e){
    this.setState({
      userPosition: e.target.value,
      errorHandlerPosition: false
    })
  }

  uploadHandle(e) {
        let _URL = window.URL || window.webkitURL;
        let file = e.target.files[0]

        let img = new Image();
        img.onload = (im) => {
            if(im.width < 70 || im.height < 70){
                this.setState({
                  errorHandlerUpload: true
                })
            }
            if(file.size > 50000) {
                this.setState({
                  errorHandlerUpload: true
                })
            } else {
                this.setState({
                    errorHandlerUpload: false,
                    text: file.name,
                    userUpload: file
                })
            }
        };
        img.src = _URL.createObjectURL(file);
  }

  openDialog(){
    this.setState({
      open: true
    })
  }
  closeDialog(){
    this.setState({
      open: false
    })
  }

  render() {
    return (
      <div className="registerForm">
        <Container maxWidth="lg" className="registerForm-container">
          <Grid container spacing={3} className="registerForm-list" justify="center" alignItems="flex-start" >
            <Grid item lg={4} md={4} sm={4} xs={12} className="input-item">
              <InputC label="Name" 
                errorHandler={this.state.errorHandlerName} 
                blurHandler={this.blurHandlerName}
                helper={this.state.helperTextName}
                placeholder={'Your name'}
              />
            </Grid>
            <Grid item lg={4} md={4} sm={4} xs={12} className="input-item">
              <InputC label="Email"
                errorHandler={this.state.errorHandlerEmail} 
                blurHandler={this.blurHandlerEmail}
                helper={this.state.helperTextEmail}
                placeholder={'Your email'}
              />
            </Grid>
            <Grid item lg={4} md={4} sm={4} xs={12} className="input-item">
              <InputC label="Phone" type="number"
                errorHandler={this.state.errorHandlerPhone} 
                blurHandler={this.blurHandlerPhone}
                helper={this.state.helperTextPhone}
              />
            </Grid>

            <Grid item lg={6} md={6} sm={6} xs={12} className="input-item">
              <SelectC positions={this.state.positions} onSelect={this.selectHandler}                 
              errorHandler={this.state.errorHandlerPosition} 
              helper={this.state.helperTextPosition}
            />
            </Grid>
            <Grid item lg={6} md={6} sm={6} xs={12} className="input-item" >
              <UploadC helper={this.state.helperTextUpload} onChange={this.uploadHandle} errorHandler={this.state.errorHandlerUpload} text={this.state.text}/>
            </Grid >
          </Grid> 

          <CircularProgress  className="loader" style={{display: this.state.loaderDisplay}}/>
          <div style={{display: this.state.buttonDisplay}}>
              <ButtoC variantType="primary" text="Sign Up" type="submit" onClick={this.submitHandler} />
          </div>
        </Container>
        <AlertDialog handleClose={this.closeDialog} onSubmitHandle={this.props.onSubmit} open={this.state.open}/>
      </div>
    )
  }
}

export default RegisterForm

import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputBase from '@material-ui/core/InputBase';
import './SelectC.scss'

const BootstrapInput = withStyles(theme => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #BDBDBD',
    fontSize: 16,
    padding: '17.5px 14px 17.5px 14px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
        borderRadius: 4,
        backgroundColor: 'white'
    },
  },
  error: {
    '& .MuiSelect-root': {
        borderColor: '#f44336',
        color: '#f44336'

    }
    //   backgroundColor: 'red',
      
  }
}))(InputBase);

export default class SelectC extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: 0
        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event) {
        this.setState({
            value: event.target.value
        })
        this.props.onSelect(event)
    };

    render() {
        return (
            <div >
                <Select
                style={{width: '100%', textAlign: 'left'}}
                  value={this.state.value}
                  onChange={this.handleChange}
                  input={<BootstrapInput error={this.props.errorHandler}/>}
                >
                  <MenuItem value={0} disabled style={{display: 'none'}}>
                    Select your position
                  </MenuItem>
                    {this.props.positions.map((pos, i) => (
                        <MenuItem key={i} value={pos.id}>{pos.name}</MenuItem>
                    ))}
                </Select>
            </div>
          );
    }
}



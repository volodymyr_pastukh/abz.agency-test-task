import React from 'react';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import NumberFormat from 'react-number-format';

const CustomTextField = withStyles({
    root: {
      '& label.Mui-focused': {
        color: '#B7B7B7',
      },
      '& label': {
        color: '#BDBDBD',
      },
      '& label.Mui-focused.Mui-error': {
        color: '#f44336',
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: '#BDBDBD',
        },
        '&:hover fieldset': {
            borderColor: '#B7B7B7',
          },
          '&.Mui-error fieldset': {
            borderColor: '#f44336',
          },
        '&.Mui-focused fieldset': {
          borderColor: '#B7B7B7',
        },
        '&.Mui-focused.Mui-error fieldset': {
            borderColor: '#f44336',
          },
      },
    },
    helperText: {
      margin: '8px 0px 8px 14px'
    }
  })(TextField);

export default function InputC(props) {
    
  if (props.type === 'number'){
      return (
        <NumberFormat 
          customInput={CustomTextField} 
          variant="outlined" label={props.label}  
          format="+38(0##) ### ####" mask="_" 
          style={{width: '100%'}}
          error={props.errorHandler}
          helperText={props.helper} 
          onBlur={props.blurHandler}
          placeholder="+38(0__) ___ ____"
        />
    )
  } else {
      return (
        <CustomTextField
            label={props.label}
            helperText={props.helper} 
            variant="outlined" 
            disabled={props.disabled}
            style={{width: '100%'}}
            error={props.errorHandler}
            onBlur={props.blurHandler}
            placeholder={props.placeholder}
        />
    )
  }
    
}
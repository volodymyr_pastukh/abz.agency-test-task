import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import style from  './UploadC.module.scss';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const CustomTextField = withStyles({
    root: {
      '& label.Mui-focused': {
        color: '#B7B7B7',
      },
      '& label': {
        color: '#BDBDBD',
      },
      '& label.Mui-focused.Mui-error': {
        color: '#f44336',
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: '#BDBDBD',
          borderRight: 'none',
          borderRadius: '4px 0px 0px 4px'
        },
        '&:hover fieldset': {
            borderColor: '#B7B7B7',
          },
        '&.Mui-focused.Mui-error fieldset': {
          border: ' 1px solid #f44336',
          
        },
        '&.Mui-focused.Mui-error.Mui-disabled fieldset ': {
            borderColor: '#f44336',

          },
      },
    },
  })(TextField);
  
export class UploadC extends Component {

    constructor(props) {
        super(props)

        this.state = {
          displayText: 'block',
        }
    }

    componentDidMount() {
      const isMobile = window.innerWidth <= 767;
        if(isMobile){
            this.setState({
              displayText: 'none',
            })
        } 
    }

    render() {
        return (
            <div className={style.customUploadWrap} >
                <CustomTextField variant="outlined"  placeholder={this.props.text} style={{width:'100%'}} helperText={this.props.helper} error={this.props.errorHandler} InputProps={{
            readOnly: true,
          }}/>
              <input
                  accept=".jpg"
                  style={{ display: 'none' }}
                  id="raised-button-file"
                  multiple
                  type="file"
                  onChange={this.props.onChange}
              />
                  <uploadIcon></uploadIcon>
                  <label htmlFor="raised-button-file">
                  <Button className={style.customUpload} component="span">
                      <span style={{display: this.state.displayText}}>Upload</span> 
                  </Button>
                  </label> 
            </div>
        )
    }
}

export default UploadC

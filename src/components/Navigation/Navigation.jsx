import React, { Component } from 'react';
import Container from '@material-ui/core/Container';
import Drawer from '@material-ui/core/Drawer';
import Fab from '@material-ui/core/Fab';
import usersAPI from '../../services/usersAPI';
import imgPerson from '../../assets/img/Person.jpg'

import { ReactComponent as LOGO } from '../../assets/img/logo/logo.svg';
import { ReactComponent as BurgerIcon } from '../../assets/img/icons/line-menu.svg';

import './Navigation.scss'


export default class Navigation extends Component {
    constructor(props) {
        super(props) 

        this.state = {
            imgUrl: imgPerson,
            open: false
        }
        this.toggleDrawer = this.toggleDrawer.bind(this)
    }

    componentDidMount() {
        usersAPI.getUser(1).then((res) => {
            this.setState({
                userName: res.data.user.name,
                userEmail: res.data.user.email,
            })
            let img = new Image()
                img.src = res.data.user.photo;
                img.onload = () => {
                    this.setState({
                        imgUrl: res.data.user.photo,
                    })
                }
        })
    }

    toggleDrawer = (open) => event => {
        this.setState({
            open: open
        })
    };

    render() { 
        return (
            <div className="navigation">
            <Container maxWidth="lg" className="navigation-container">
                <div className="navigation-wrap">
                    <a href="# " style={{fontSize: '0px', display: 'block'}}><LOGO className="navigation-logo"></LOGO></a>
                    <nav className="navigation-list">
                        <a href="#about" className="par2">About me</a>
                        <a href="#relationships" className="par2">Relationships</a>
                        <a href="#requirements" className="par2">Requirements</a>
                        <a href="#users" className="par2">Users</a>
                        <a href="#signup" className="par2">Sign Up</a>
                    </nav>
                    <div className="navigation-user">
                        <div className="user-info">
                            <p className="user-name">{this.state.userName}</p>
                            <p className="user-email">{this.state.userEmail}</p>
                        </div>
                        <img src={this.state.imgUrl} alt='abz user' className="user-img"/>
                        <div className="logout-button"></div>
                    </div>

                    <Fab onClick={this.toggleDrawer(true)}>
                        <BurgerIcon />
                    </Fab>

                    <Drawer open={this.state.open} onClose={this.toggleDrawer(false)}>
                        <div className="menu">
                            <div className="menu-wrap">
                                <img src={this.state.imgUrl} alt='abz user' className="user-img" /> 
                                <div className="user-info">
                                    <p className="user-name">
                                        {this.state.userName}
                                    </p>
                                    <p className="user-email">
                                        {this.state.userEmail}
                                    </p>
                                </div>
                                <hr></hr>
                                <nav>
                                    <a href="#about" className="par2" onClick={this.toggleDrawer(false)}>About me</a>
                                    <a href="#relationships" className="par2" onClick={this.toggleDrawer(false)}>Relationships</a>
                                    <a href="#requirements" className="par2" onClick={this.toggleDrawer(false)}>Requirements</a>
                                    <a href="#users" className="par2" onClick={this.toggleDrawer(false)}>Users</a>
                                    <a href="#signup" className="par2" onClick={this.toggleDrawer(false)}>Sign Up</a>
                                    <a href="#signup" className="par2" onClick={this.toggleDrawer(false)}>Sign Out</a>
                                </nav>
                            </div>
                        </div>
                    </Drawer>
                </div>
            </Container>
        </div>
        )
    }
}




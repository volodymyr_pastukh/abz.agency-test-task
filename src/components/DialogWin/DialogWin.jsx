import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import style from './DialogWin.module.scss'

export default function AlertDialog(props) {
  return (
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        className={style.dialogWin}
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
          <h3 style={{color: 'black'}}>Congratulations</h3>
            You have successfully passed the registration
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={()=>{props.onSubmitHandle(); props.handleClose()}} color="primary" autoFocus style={{span:{color:'black'}}}>
            <h5>Ok</h5>
          </Button>
        </DialogActions>
      </Dialog>
  );
}
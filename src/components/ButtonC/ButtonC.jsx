import React from 'react';
import Button from '@material-ui/core/Button';
import styles from './ButtonC.module.scss'

export default function ButtonC(props) {

    if (props.variantType === "primary") {
        return (      
            <Button disabled={props.disabled} className={styles.customButton_primary} onClick={props.onClick} type={props.type}>{props.text}</Button>
        )
    } else if (props.variantType === "primary-outline") {
        return (     
            <Button className={styles.customButton_primary_outline} onClick={props.onClick}>{props.text}</Button>
        )
    } else if (props.variantType === "secondary") {
        return (     
            <Button className={styles.customButton_secondary} onClick={props.onClick}>{props.text}</Button>
        )
    } else if (props.variantType === "secondary-outline") {
        return (   
            <Button className={styles.customButton_secondary_outline} onClick={props.onClick}>{props.text}</Button>
        )
    }
}
import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { ReactComponent as ManLaptopV1 } from '../../../assets/img/man-pic/man-laptop-v1.svg';
import { ReactComponent as ManLaptopV2 } from '../../../assets/img/man-pic/man-laptop-v2.svg';

import './Requirements.scss'

export default function Requirements() {
    return (
        <section className="main-section requirements">
            <div id="requirements" className="navigationSelector"> </div>
            <Container maxWidth="lg">

                <h2 className='requirements-header'>General requirements for the test task</h2>

                <Grid container spacing={3} className="requirements-content" justify="center">
                    <Grid item lg={6} className="requirements-text">
                        <p className="par2">
                            Users want to find answers to their questions quickly and data 
                            shows that people really care about how quickly their pages load. 
                            The Search team announced speed would be a ranking signal for 
                            desktop searches in 2010 and as of this month (July 2018), 
                            page speed will be a ranking factor for mobile searches too.
                            <br></br>
                            <br></br>
                            If you're a developer working on a site, now is a good time to evaluate 
                            your performance using our speed tools. Think about how performance 
                            affects the user experience of your pages and consider measuring a variety 
                            of real-world user-centric performance metrics.
                            <br></br>
                            <br></br>
                            Are you shipping too much JavaScript? Too many images? Images and JavaScript 
                            are the most significant contributors to the page weight that affect page load 
                            time based on data from HTTP Archive and the Chrome User Experience Report - 
                            our public dataset for key UX metrics as experienced by Chrome users under 
                            real-world conditions."
                        </p>
                    </Grid>
                    <Grid item lg={6} className="requirements-img">
                        <ManLaptopV1 className="manLaptopV1"></ManLaptopV1>
                        <ManLaptopV2 className="manLaptopV2"></ManLaptopV2>
                    </Grid>
                </Grid>
            </Container>
        </section>
    )
}


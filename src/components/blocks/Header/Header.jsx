import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import ButtonC from '../../ButtonC/ButtonC'

import './Header.scss'

export default function Header() {
    return (
        <section className="main-section header">
            <Container maxWidth="lg">
                <Grid container spacing={3} className="header-content" alignItems="center">
                    <Grid item lg={5} md={6} sm={7}>
                        <h1>
                            Test assignment for Frontend Developer position
                        </h1>
                        <p className="par1">
                            We kindly remind you that your test assignment should 
                            be submitted as a link to github/bitbucket repository. 
                            Please be patient, we consider and respond to every application 
                            that meets minimum requirements. We look forward to your submission. 
                            Good luck!
                        </p>
                        <a href='#signup' style={{color: 'white'}}>
                            <ButtonC variantType="primary" text="Sign Up"/>
                        </a>
                    </Grid>
                </Grid>
            </Container>
        </section>
    )
}
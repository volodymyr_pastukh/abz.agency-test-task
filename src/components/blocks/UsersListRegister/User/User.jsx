import React, { Component } from 'react'
import Tooltip from '@material-ui/core/Tooltip';
import Grid from '@material-ui/core/Grid';
import imgPerson from '../../../../assets/img/Person.jpg'

export default class User extends Component {

    constructor(props) {
        super(props)

        this.state = {
            toolName: false,
            toolEmail: false,
            id: this.props.id,
            imgUrl: imgPerson
        }
    }

    componentDidMount() {
        let els = document.querySelector(`#a${this.state.id}`)
        if(els.scrollWidth > els.clientWidth) {
            this.setState({
                toolName: true
            })
        }
            els = document.querySelector(`#b${this.state.id}`)
        if(els.scrollWidth > els.clientWidth) {
            this.setState({
                toolEmail: true
            })
        }

        let img = new Image()
        img.src = this.props.user.photo;
        
        img.onload = () => {
            this.setState({
                imgUrl: this.props.user.photo
            })
        }
    }
    
    render() {
        const { name, position, email, phone } = this.props.user;

        return (
            <Grid item lg={4} md={4} sm={4} xs={12} style={{textAlign: 'right'}} className="user">
                <div className="user-img">
                    <img src={this.state.imgUrl} alt={name} height="100%" onError={(e)=>{e.target.onerror = null; e.target.src="https://icons-for-free.com/iconfiles/png/512/boy+guy+man+icon-1320166733913205010.png"}}/>
                </div>
                <div className="user-info">
                    <Tooltip className="userName" title={name} disableHoverListener={!this.state.toolName}>
                        <h3 id={'a'+this.state.id}>{name}</h3>
                    </Tooltip>
                    <p className="par3">{position}</p>
                    <Tooltip title={email} disableHoverListener={!this.state.toolEmail}>
                        <p id={'b'+this.state.id}  className="par3">{email}</p>
                    </Tooltip>
                    <p className="par3">{phone}</p>
                </div>
            </Grid>
        )
    }
}

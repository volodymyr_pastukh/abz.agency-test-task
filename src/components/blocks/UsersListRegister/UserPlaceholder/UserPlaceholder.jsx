import React from 'react'
import Grid from '@material-ui/core/Grid';
import imgPerson from '../../../../assets/img/Person.jpg'

export default function UserPlaceholder() {
        return (
            <Grid item lg={4} md={4} sm={4} xs={12} style={{textAlign: 'right'}} className="user">
                <div className="user-img">
                    <img src={imgPerson} alt={imgPerson} height="100%"/>
                </div>
                <div className="user-info">
                    <h3 style={{width: '90%', height:'15px', backgroundColor:"#5079B7"}}> </h3>
                    <p style={{width: '100%', height:'15px', backgroundColor:"grey"}} className="par3"> </p>
                    <p style={{width: '60%', height:'15px', backgroundColor:"#EF6C00"}} className="par3"> </p>
                    <p style={{width: '70%', height:'15px', backgroundColor:"grey"}} className="par3"> </p>
                </div>
            </Grid>
        )
}

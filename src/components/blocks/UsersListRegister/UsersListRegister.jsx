import React, { Component } from 'react'

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';

import ButtonC from '../../ButtonC/ButtonC'
import User from './User/User';
import UserPlaceholder from './UserPlaceholder/UserPlaceholder'
import RegisterForm from '../../RegisterForm/RegisterForm'
import usersAPI from '../../../services/usersAPI'

import './UsersListRegister.scss'

export default class UsersListRegister extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoad: false,
            users: [],
            buttonDisplay: 'none',
            loaderDisplay: 'block',
            page: 0,
            total_pages: 0,
            count: 6
        };
        this.showMore = this.showMore.bind(this)
        this.reset = this.reset.bind(this)
    }

    componentDidMount() {
        this.showMore();
    }

    reset() {
        this.setState({
            isLoad: false,
            users: [],
            buttonDisplay: 'none',
            loaderDisplay: 'block',
            page: 0,
            total_pages: 0,
            count: 6
        }, () => {this.showMore();});
    }

    showMore() {
        let count = this.state.count;
        this.setState({
            buttonDisplay: 'none',
            loaderDisplay: 'block'
        })

        const isMobile = window.innerWidth <= 600;
        if(isMobile){
            count = count/2
        } 

        usersAPI.getUsers(Math.floor(this.state.page)+1,count).then((res) => {
            this.setState({
                page: res.data.page,
                total_pages: res.data.total_pages
            })
            // this.state.page = res.data.page;
            // this.state.total_pages = res.data.total_pages;
            
            return res.data.users.sort((a , b) => 
                a.registration_timestamp < b.registration_timestamp ? 1 : -1
            )
        }).then((users) => {
            let newUsers = this.state.users.concat(users)

            if(this.state.page === this.state.total_pages) {
                this.setState({
                    isLoad: true,
                    buttonDisplay: 'none',
                    loaderDisplay: 'none',
                    users: newUsers
                })
            }else {
                this.setState({
                    isLoad: true,
                    buttonDisplay: 'inline',
                    loaderDisplay: 'none',
                    users: newUsers
                })
            }
        })
    }
    render() {
        const { isLoad, users, buttonDisplay, loaderDisplay } = this.state;

        if(isLoad) {
            return (
                <section className="main-section usersList_register">
                    <div id="users" className="navigationSelector"> </div>
                    <Container maxWidth="lg" className="usersList_register-container">
                        <h2 className="usersList-header">Our cheerful users</h2>
                        <h5>Attention! Sorting users by registration date</h5>
                        
                        <Grid container spacing={3} className="usersList" justify="center" alignItems="flex-start" >
                            {users.map((user,i) => (
                                <User user={user} key={i} id={i}/>
                            ))}
                        </Grid> 

                        <CircularProgress  className="loader" style={{display: loaderDisplay}}/>
                        <div style={{display: buttonDisplay}}>
                            <ButtonC variantType="primary-outline" onClick={this.showMore}  text="Show more" />
                        </div>     
                                                    
                        <div id="signup" className="navigationSelector"> </div>

                        <h2 className="register-header">Register to get a work</h2>
                        <h5 className="register-header-caption" >Attention! After successful registration and alert, update the list of users in the block from the top</h5>
                        <RegisterForm onSubmit={this.reset}/>
                        
                    </Container >
                </section >
            )
        } else {
            return (
                <section className="main-section usersList_register">
                    <Container maxWidth="lg">
                        <h2 className="usersList-header">Our cheerful users</h2>
                        <h5 >Attention! Sorting users by registration date</h5>
                        <Grid container spacing={3} className="usersList" justify="center" alignItems="flex-start" >
                            {[1,2,3,4,5,6].map((i) => (
                                <UserPlaceholder  key={i} />
                            ))}
                        </Grid> 
                        <CircularProgress  className="loader" style={{display: loaderDisplay}}/>
                        <h2 className="register-header">Register to get a work</h2>
                        <h5 className="register-header-caption">Attention! After successful registration and alert, update the list of users in the block from the top</h5>
                        <RegisterForm onSubmit={this.reset}/>
                    </Container>
                </section>
            )
        }
    }
}

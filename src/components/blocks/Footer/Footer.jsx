import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

import img from '../../../assets/img/logo/logo_white.png';
import { ReactComponent as Mail } from '../../../assets/img/icons/mail.svg';
import { ReactComponent as Phone1 } from '../../../assets/img/icons/phone.svg';
import { ReactComponent as Phone2 } from '../../../assets/img/icons/cellphone.svg';

import { ReactComponent as Facebook } from '../../../assets/img/icons/facebook.svg';
import { ReactComponent as Instagram } from '../../../assets/img/icons/instagram.svg';
import { ReactComponent as Linkedin } from '../../../assets/img/icons/linkedin.svg';
import { ReactComponent as Twitter } from '../../../assets/img/icons/twitter.svg';
import { ReactComponent as Pinterest } from '../../../assets/img/icons/pinterest.svg';

import './Footer.scss'

export default function Footer() {
    return (
        <footer className="footer">
            <hr color="#5F5FC4" size={1}/>
            <Container maxWidth="lg" className="footer-container">
                <div className="fotter_navigation">
                    <a href="# "><img src={img} alt="abz.agency logo" className="footer_navigation-logo"/></a>
                    <nav className="fotter_navigation-list">
                        <a href="#about">About me</a>
                        <a href="#relationships">Relationships</a>
                        <a href="#requirements">Requirements</a>
                        <a href="#users">Users</a>
                        <a href="#signup">Sign Up</a>
                    </nav>
                </div>
                <Grid container spacing={3} className="footer_info" justify="space-between" >
                    <Grid md={4} className="footer-contact">
                        <div className="email">
                            <Mail />
                            <a href="mailto:work.of.future@email.com" className="par1">work.of.future@email.com</a>
                        </div>
                        <div className="phone_1">
                            <Phone1 />
                            <a href="tel:0507892498" className="par1">+38 (050) 789 24 98</a>
                        </div>
                        <div className="phone_2">
                            <Phone2 />
                            <a href="tel:0955560845" className="par1">+38 (095) 556 08 45</a>
                        </div>
                    </Grid>
                    
                    <Grid md={2} className="footer-category">
                        <a href="# ">News</a>
                        <a href="# ">Blog</a>
                        <a href="# ">Partners</a>
                        <a href="# ">Shop</a>
                    </Grid>
                    <Grid md={2} className="footer-category">
                        <a href="# ">Overview</a>
                        <a href="# ">Design</a>
                        <a href="# ">Code</a>
                        <a href="# ">Collaborate</a>
                    </Grid>
                    <Grid md={2} className="footer-category">
                        <a href="# ">Tutorials</a>
                        <a href="# ">Resources</a>
                        <a href="# ">Guides</a>
                        <a href="# ">Examples</a>
                    </Grid>
                    <Grid md={2} className="footer-category">
                        <a href="# ">FAQ</a>
                        <a href="# ">Terms</a>
                        <a href="# ">Conditions</a>
                        <a href="# ">Help</a>
                    </Grid>
                </Grid>
                <div className="footer_bottom">
                    <p className="copyright par3">© abz.agency specially for the test task</p>
                    <div className="soc">
                        <a href="# "><Facebook /></a>
                        <a href="# "><Linkedin /></a>
                        <a href="# "><Instagram /></a>
                        <a href="# "><Twitter /></a>
                        <a href="# "><Pinterest /></a>
                    </div>
                </div>
            </Container>
        </footer>
    )
}

import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

import { ReactComponent as ManMobile } from '../../../assets/img/man-pic/man-mobile.svg';
import { ReactComponent as CSS } from '../../../assets/img/css.svg';
import { ReactComponent as HTML } from '../../../assets/img/html.svg';
import { ReactComponent as JS } from '../../../assets/img/javascript.svg';

import './AboutRelat.scss'

export default function AboutRelat() {
    return (
        <section className="main-section about_relat" >
            <div id="about" className="navigationSelector"> </div>
            <Container maxWidth="lg" >

                <h2 className="about_relat-content_1-header">Let's get acquainted</h2>
              
                <Grid container spacing={3} className="about_relat-content_1" justify="center" alignItems="flex-start" >
                    <Grid item lg={4} md={4} sm={4} style={{textAlign: 'right'}}>
                        <ManMobile className="manMobile"></ManMobile>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} className="about_relat-content-text">
                        <h3>I am cool frontend developer</h3>
                        <p className="par2">
                            When real users have a slow experience on mobile, 
                            they're much less likely to find what they are looking 
                            for or purchase from you in the future. For many sites 
                            this equates to a huge missed opportunity, especially 
                            when more than half of visits are abandoned if a mobile 
                            page takes over 3 seconds to load. 
                            <br></br>
                            <br></br>
                            Last week, Google Search and Ads teams announced two 
                            new speed initiatives to help improve user-experience on the web. 
                        </p>
                        <a href="#signup">Sign Up</a>
                    </Grid>
                </Grid>
                <div id="relationships" className="navigationSelector"> </div>
                <h2 className="about_relat-content_2-header" >
                    About my relationships with <br></br>
                    web-development 
                </h2>
              
                <Grid container spacing={3} className="about_relat-content_2" >
                    <Grid item lg={4} md={4} sm={12} className="card">
                        <HTML className="langImg"></HTML>
                        <div className="card-text">
                            <h3>I'm in love with HTML</h3>
                            <p className="par2">
                                Hypertext Markup Language (HTML) is the 
                                standard markup language for creating web pages 
                                and web applications.
                            </p>
                        </div>
                    </Grid>
                    <Grid item lg={4} md={4} sm={12} className="card">
                        <CSS className="langImg"></CSS>
                        <div className="card-text">
                            <h3>CSS is my best friend</h3>
                            <p className="par2">
                                Cascading Style Sheets (CSS) is a  style sheet language used for 
                                describing the presentation 
                                of a document written in a markup language 
                                like HTML.
                            </p>
                        </div>
                    </Grid>
                    <Grid item lg={4} md={4} sm={12} className="card">
                        <JS className="langImg"></JS>
                        <div className="card-text">
                            <h3>JavaScript is my passion</h3>
                            <p className="par2">
                                JavaScript is a high-level, interpreted programming language. 
                                It is a language which is also characterized as dynamic, weakly typed, 
                                prototype-based and multi-paradigm.
                            </p>
                        </div>
                    </Grid>
                </Grid>
            </Container>
        </section>
    )
}

import React from 'react';

import UserListRegister from './components/blocks/UsersListRegister/UsersListRegister'
import Requirements from './components/blocks/Requirements/Requirements'
import Footer from './components/blocks/Footer/Footer'

export default function OtherBlocks () {
    return (
        <>
            <Requirements />
            <UserListRegister />
            <Footer />
        </>
    )
}
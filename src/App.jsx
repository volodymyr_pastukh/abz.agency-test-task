import React, { Suspense } from 'react';
import './GlobalStyles.scss'

import Navigation from './components/Navigation/Navigation'
import Header from './components/blocks/Header/Header'
import AboutRelat from './components/blocks/AboutRelat/AboutRelat'

const OtherBlocksLazy = React.lazy(() => import('./OtherBlocks'));



export default function App() {
  return (
    <div className="app">
      <Navigation />
      <Header />
      <AboutRelat />

      <Suspense fallback={<div>Загрузка...</div>}>
        <OtherBlocksLazy />
      </Suspense>
    </div>
  );
}


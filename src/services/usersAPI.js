import axios from 'axios';

const usersAPI = {

    getToken: () => {
        return axios.get('https://frontend-test-assignment-api.abz.agency/api/v1/token')
    },

    getUsers: (page, count) => {
        return axios.get(`https://frontend-test-assignment-api.abz.agency/api/v1/users?page=${page || 1}&count=${count || 6}`)
    },

    getUser: (id) => {
        return axios.get(`https://frontend-test-assignment-api.abz.agency/api/v1/users/${id}`)
    },

    getPositions: () => {
        return axios.get(`https://frontend-test-assignment-api.abz.agency/api/v1/positions`)
    }

}

export default usersAPI

